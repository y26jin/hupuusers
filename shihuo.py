__author__ = 'yuhang'
# -*- coding: utf-8 -*-
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import threading
import ConfigParser
import requests
import pymysql
import BeautifulSoup

class ShihuoScrape:

    def __init__(self):
        self.cf = ConfigParser.ConfigParser()
        self.cf.read('config.ini')
        self.URL = self.cf.get('shihuo', 'URL')
        self.max_num_pages = int(self.cf.get('shihuo','max_number_pages'))

        self.session = requests.session()

    def scrape_OnePage(self, page=1):
        item_url = self.URL+str(page)+".html"
        try:
            item_page = self.session.get(item_url,timeout=15).text
        except requests.ConnectionError as e:
            print e.message
            return
        except requests.Timeout as ee:
            print ee.message
            return

        soup = BeautifulSoup.BeautifulSoup(item_page)

        # 物品基本信息，查看是否存在该物品
        detail_area = soup.findAll('div',{'class':'area-sub'})

        for row in detail_area:
            ret = row.findAll('div',{'class':'area-sub-min clearfix'})
            if ret:
                # 不存在该物品，自动返回了首页
                with open('invalid_pages.log','a') as file:
                    file.write(str(page)+',')
                return

        detail_attrs = []
        # 物品基本信息：名称
        item_title = ''
        for row in detail_area:
            ret = row.findAll('h1')
            if ret:
                item_title = ret[0].text
            else:
                ret = row.findAll('h2')
                if ret:
                    item_title = ret[0].text

        for row in detail_area:
            ret=row.findAll('div')
            for r in ret:
                if r:
                    detail_attrs.append(r)

        # 物品基本信息：特征/标签
        tags = []
        tag_html = detail_attrs[0]
        for tg in tag_html:
            if tg == '\n':
                continue
            for t in tg:
                tags.append(t)

        # 物品基本信息：推荐理由，品牌，类型，型号
        item_reason = ""
        item_brand = ""
        item_category = ""
        item_sub_kind = ""
        temp_html = detail_attrs[1]

        for tg in temp_html:
            if tg == '\n':
                continue
            ret = tg.findAll('span')
            item_reason = ret[1].getText()
            item_brand = ret[3].getText()
            item_category = ret[5].getText()
            item_sub_kind = ret[7].getText()

        # 物品基本信息：价格（币种，价格）
        item_price = []
        for pr in detail_attrs[2]:
            if pr == '\n':
                continue
            item_price.append(pr.string)
        item_price = item_price[1:]

        # 物品基本信息：能不能买
        item_buyable = True
        for row in detail_area:
            ret = row.findAll('a',{'class':'go-buy2'})
            if not ret:
                item_buyable = False

        # 判定是否是无效页面
        invalid_page = False
        if item_buyable==False:
            invalid_page = True
        elif not item_brand or not item_category:
            invalid_page = True

        if invalid_page:
            with open('invalid_pages.log','a') as file:
                file.write(str(page)+',')

if __name__=="__main__":
    shihuo = ShihuoScrape()
    #shihuo.scrape_OnePage(8)
    # 先刷一遍所有物品，找出所有的无效页面
    for i in range(1,shihuo.max_num_pages):
        print i,
        shihuo.scrape_OnePage(i)

