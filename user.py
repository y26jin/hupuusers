__author__ = 'yuhang'
# -*- coding: utf-8 -*-
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import threading
import ConfigParser
import requests
import pymysql
import BeautifulSoup

class UserConnector(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self, name='hupuuserconnector')

    def run(self):
        threads = []

        cf = ConfigParser.ConfigParser()
        cf.read('config.ini')

        # log into hupu
        loginURL = cf.get('initlogin','loginURL')
        loginUsername = cf.get('initlogin','loginUsername')
        loginPassword = cf.get('initlogin', 'loginPassword')

        login_data = {'username':loginUsername, 'password':loginPassword}
        session = requests.session()
        session.post(loginURL, login_data)
        cookie = session.cookies

        thread_num = int(cf.get('thread','thread_num'))
        user_total = int(cf.get('user','userid_amount'))

        for i in range(thread_num):
            threads.append(UpdateOneUser(i*user_total/thread_num, session, cookie))

        for i in range(thread_num):
            threads[i].daemon = True

        for i in range(thread_num):
            threads[i].start()

        for i in range(thread_num):
            threads[i].join()

class UpdateOneUser(threading.Thread):
    def __init__(self,start_id, session, cookie):
        threading.Thread.__init__(self,name=str(start_id))
        self.start_id = start_id
        self.cookie = cookie
        self.session = session

        cf = ConfigParser.ConfigParser()
        cf.read('config.ini')
        thread_num = int(cf.get('thread','thread_num'))
        user_total = int(cf.get('user','userid_amount'))
        self.work_num = user_total/thread_num

        # log in to database
        host = cf.get('db','host')
        port = int(cf.get('db','port'))
        user = cf.get('db','user')
        passwd = cf.get('db','passwd')
        db = cf.get('db','db')
        charset = cf.get('db','charset')

        #self.db_conn = pymysql.connect(host=host,
                                       #port=port,
                                       #user=user,
                                       #passwd=passwd,
                                       #db=db,
                                       #charset=charset,
                                       #autocommit=True)
        #self.db_cursor = self.db_conn.cursor()

    def convert_raw_profile(self, username, basic, teams):
        profile_data = {'id':'',
                        'name':'',
                        'gender':'',
                        'birthday':'',
                        'location':'',
                        'userLevel':'',
                        'userGroup':'',
                        'userPosition':'',
                        'cash':'',
                        'onlineHour':'',
                        'registerDate':'',
                        'lastLogin':'',
                        'introduction':'',
                        'sports':'',
                        'leagues':'',
                        'teams':''}
        profile_data['name']=username
        profile_data['id']=basic[0][1]
        profile_data['gender']=basic[1][1]
        profile_data['birthday']=basic[2][1]

        index = 0
        if len(basic) == 10:
            # 没有位置信息的普通用户
            index = 1
        elif len(basic) == 11:
            # 有位置信息的普通用户
            index = 0
        elif len(basic) == 12:
            # 管理员
            index = 0

        profile_data['location']=basic[3-index][1]
        profile_data['userLevel']=basic[4-index][1]
        profile_data['userGroup']=basic[5-index][1]
        #profile_data['cash']=basic[6-index][1]

        admin = "管理员"
        if basic[6-index][1].encode('utf-8') == admin:
            profile_data['userPosition'] = 'admin'
            profile_data['onlineHour'] = '-1'
        else:
            profile_data['onlineHour']=basic[7-index][1][:-2]

        profile_data['registerDate']=basic[8-index][1]
        profile_data['lastLogin']=basic[9-index][1]
        if len(basic[10-index]) > 1:
            profile_data['introduction']=basic[10-index][1]
        if len(teams[0]) > 1:
            profile_data['sports']=teams[0][1]
        if len(teams[1]) > 1:
            profile_data['leagues']=teams[1][1]
        if len(teams[2]) > 2:
            profile_data['teams']=teams[2][1]

        return profile_data


    def run(self):
        for i in range(self.start_id, self.start_id + self.work_num):
            # retrieve the profile page
            try:
                profile_url = 'http://my.hupu.com/'
                profile_url += str(i)
                profile_url += '/profile'
                self.profile_page = self.session.get(profile_url,cookies=self.cookie,timeout=5).text
            except Exception as e:
                raise e

            if len(self.profile_page) < 10:
                # either userid doesn't exist, or server has some issues
                continue

            # parse the profile page
            soup = BeautifulSoup.BeautifulSoup(self.profile_page)
            page_head = soup.find('head')
            page_title = page_head.find('title')

            raw_data = []
            profile_username = page_title.text.strip()[:-3]
            profile_table = soup.findAll('table')

            if len(profile_table) < 2:
                # don't proceed broken pages
                continue

            profile_table_basic = profile_table[0].findAll('tr') # basic info
            profile_table_teams = profile_table[1].findAll('tr') # teams one likes
            profile_basic = []
            profile_teams = []
            for row in profile_table_basic:
                cols = row.findAll('td')
                cols = [ele.text.strip() for ele in cols]
                profile_basic.append([ele for ele in cols if ele])
            for row in profile_table_teams:
                cols = row.findAll('td')
                cols = [ele.text.strip() for ele in cols]
                profile_teams.append([ele for ele in cols if ele])
            profile_data = self.convert_raw_profile(profile_username, profile_basic, profile_teams)

            # retrieve personal space page to get page visits and bbs posts info
            try:
                space_url = 'http://my.hupu.com/'+str(i)
                self.space_page = self.session.get(space_url, cookies=self.cookie, timeout=5).text
            except Exception as e:
                raise e

            space_soup = BeautifulSoup.BeautifulSoup(self.space_page)
            space_visits = space_soup.find('h3').find('span').text.strip()
            profile_data['pagevisit'] = space_visits

            space_hrefs = space_soup.findAll('a',href=True)
            space_following = []
            space_follower = []
            space_topic_total = []
            space_topic_digest = [] # 精华帖
            for link in space_hrefs:
                if link['href'] == '/'+str(i)+'/following':
                    space_following.append(link.string)
                if link['href'] == '/'+str(i)+'/follower':
                    space_follower.append(link.string)
                if link['href'] == '/'+str(i)+'/topic':
                    space_topic_total.append(link.string)
                if link['href'] == '/'+str(i)+'/topic-digest-1':
                    space_topic_digest.append(link.string)
            profile_data['following'] = space_following
            profile_data['follower'] = space_follower
            profile_data['topic'] = space_topic_total
            profile_data['digesttopic'] = space_topic_digest
            if int(profile_data['onlineHour'])<10:
                print profile_data['name']+","+profile_data['userLevel']



if __name__=="__main__":
    connector = UserConnector()
    connector.start()
